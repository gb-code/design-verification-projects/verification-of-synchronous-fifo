/*
	1. extends base test.
	2. starts the virtual sequence.
*/

class virtualtest extends top_test_base;
    `uvm_component_utils(virtualtest)

	vseq_rst_data vseq_h;
  
  function new(string name = "virtualtest", uvm_component parent);   
        super.new(name, parent); 
    endfunction
    
    function void build_phase(uvm_phase phase);
      	super.build_phase(phase);
        vseq_h = vseq_rst_data::type_id::create("vseq_h"); 
    endfunction 

    task run_phase(uvm_phase phase);
        phase.raise_objection(this);
        init_vseq(vseq_h);
        vseq_h.start(null);
        phase.drop_objection(this);
    endtask    
endclass : virtualtest  
