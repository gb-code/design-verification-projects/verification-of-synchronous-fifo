/*
	Passive Agent contains output sampling monitor (Monitor 2).
*/

class my_agent_3 extends uvm_component;
    `uvm_component_utils(my_agent_3)
    
  	uvm_analysis_port #(data_transaction) ap; 
    my_monitor_2 monitor_2_h;    
    
    function new(string name, uvm_component parent);
        super.new(name, parent); 
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        monitor_2_h = my_monitor_2::type_id::create("monitor_2_h", this);
    endfunction

    function void connect_phase(uvm_phase phase);
        ap = monitor_2_h.ap; 
    endfunction
endclass : my_agent_3 