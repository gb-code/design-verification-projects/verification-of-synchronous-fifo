/***********************************************
* virtual sequencer 
***********************************************/
class top_vseq_base extends uvm_sequence #(uvm_sequence_item);
    `uvm_object_utils(top_vseq_base)
    
    rst_sequencer rst_sqr_h;
    data_sequencer data_sqr_h;
    
    function new(string name = "top_vseq_base");
        super.new(name);
    endfunction 
endclass