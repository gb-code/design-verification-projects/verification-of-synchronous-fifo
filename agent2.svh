/*
	1. Responsible for containing reset driver, reset sequencer.
*/

class my_agent_2 extends uvm_component;
    `uvm_component_utils(my_agent_2)

    rst_driver rst_driver_h; 
    rst_sequencer rst_sequencer_h;
 
    function new(string name, uvm_component parent);
        super.new(name, parent); 
    endfunction

    function void build_phase(uvm_phase phase);
        rst_driver_h    = rst_driver::type_id::create("rst_driver_h", this);
        rst_sequencer_h = rst_sequencer::type_id::create("rst_sequencer_h", this); 
    endfunction

    function void connect_phase(uvm_phase phase);
        rst_driver_h.seq_item_port.connect(rst_sequencer_h.seq_item_export);
    endfunction 
endclass : my_agent_2
