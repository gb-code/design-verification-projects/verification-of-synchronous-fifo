class rst_driver extends uvm_driver #(rst_transaction);
    `uvm_component_utils(rst_driver)
    
    rst_transaction tr; 
    virtual dut_if driver2dut;

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction
    
    function void build_phase(uvm_phase phase);
        if (!uvm_config_db #(virtual dut_if)::get(this, "", "dut_if", driver2dut))
            `uvm_info("RST_DRIVER", "uvm_config_db::get failed!", UVM_HIGH)
    endfunction    
    
    task run_phase(uvm_phase phase);
        seq_item_port.get_next_item(tr); 
        driver2dut.reset = tr.rst; 
        repeat (3) @(negedge driver2dut.clock);
        seq_item_port.item_done();

        seq_item_port.get_next_item(tr); 
        driver2dut.reset = tr.rst; 
        seq_item_port.item_done();
    endtask
endclass : rst_driver
