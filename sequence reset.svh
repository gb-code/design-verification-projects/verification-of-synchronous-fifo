class FIFO_rst_sequence extends uvm_sequence #(rst_transaction);
    `uvm_object_utils(FIFO_rst_sequence)
    
    rst_transaction tr; 
    
  function new(string name = "rst_seq");
        super.new(name); 
    endfunction

    task body();
        tr = rst_transaction::type_id::create("rst_tx_tr");
        start_item(tr); 
        tr.rst = 1'b1;
        finish_item(tr); 

        tr = rst_transaction::type_id::create("rst_tx_tr");
        start_item(tr); 
        tr.rst = 1'b0;
        finish_item(tr); 
    endtask   
endclass
