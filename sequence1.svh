// FIFO test 1
// write FIFO from empty to full -> read FIFO from full to empty
class FIFO_test_1_sequence extends uvm_sequence #(data_transaction); 
    `uvm_object_utils(FIFO_test_1_sequence)
    
    data_transaction tr;

  function new(string name = "data_seq_1");
        super.new(name); 
    endfunction
    
    task body();
        for (int i = 0; i < 40; i++) begin 
            tr = data_transaction::type_id::create("tx_data_tr");
            start_item(tr);
            
            if (i < 20) begin
                if (!tr.randomize() with {tr.put == 1'b1; tr.get == 1'b0;}) begin
                    `uvm_error("Sequence", "Randomization failure for trasaction")
                end 
            end
            else begin
                if (!tr.randomize() with {tr.put == 1'b0; tr.get == 1'b1;}) begin
                    `uvm_error("Sequence", "Randomization failure for trasaction")
                end 
            end

            finish_item(tr); 
        end
    endtask
endclass : FIFO_test_1_sequence   
